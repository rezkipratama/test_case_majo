import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/local/dao/user_dao.dart';
import 'package:majootestcase/local/database.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/services/repositories/movie_repository.dart';
import 'package:majootestcase/utils/constant.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  await providerDataModule();
  provideBlocModule();
}

Future providerDataModule() async {

  Dio _dioInstance = Dio(BaseOptions(
      connectTimeout: 20000, receiveTimeout: 20000, baseUrl: Api.baseUrl));

  final database =
      await $FloorAppDatabase.databaseBuilder('radian_database.db').build();
  final userDao = database.userDao;

  locator.registerSingleton<ApiServices>(ApiServices(_dioInstance));

  locator.registerSingleton<UserDao>(userDao);

  locator.registerFactory(
      () => MovieRepository(locator<ApiServices>()));
}

provideBlocModule() {
  locator.registerFactory(() => AuthBlocCubit(locator<UserDao>()));
  locator.registerFactory(() => HomeBlocCubit(locator<MovieRepository>()));
}
