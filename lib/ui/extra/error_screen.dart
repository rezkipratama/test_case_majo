import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.error, size: 70, color: Colors.white,),
            Text(
              message,
              style: Font.txtWhiteBoldMD,
              textAlign: TextAlign.center,
            ),
            retry != null
                ? Column(
                    children: [
                      retryButton ??
                          IconButton(
                            iconSize: 30,
                            onPressed: () {
                              if (retry != null) retry!();
                            },
                            icon: Icon(Icons.refresh_sharp, color: Colors.white,),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
