import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_page.dart';
import 'package:majootestcase/utils/constant.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  const HomeBlocLoadedScreen({Key? key, this.data = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: RefreshIndicator(
        onRefresh: () async {
          await Future.delayed(const Duration(seconds: 1));
          context.read<HomeBlocCubit>().fetchingData();
        },
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailMoviePage(data: data[index])));
                },
                child: movieItemWidget(data[index]));
          },
        ),
      ),
    );
  }

  Widget movieItemWidget(Results data) {
    return Card(
      color: Colors.black,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(2),
            child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    bottomLeft: Radius.circular(20.0),
                    topRight: Radius.circular(5),
                    bottomRight: Radius.circular(5)),
                child: Hero(
                  tag: data.id.toString(),
                  child: Image.network(
                      "https://image.tmdb.org/t/p/w500/" + data.posterPath!,
                      height: 120,
                      fit: BoxFit.cover),
                )),
          ),
          Flexible(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(data.originalTitle ?? "unknown",
                      style: Font.txtWhiteBoldLG,
                      textDirection: TextDirection.ltr),
                  SizedBox(height: 5),
                  Text(data.voteAverage.toString(),
                      style: Font.txtWhiteBoldSM,
                      textDirection: TextDirection.ltr),
                  SizedBox(height: 5),
                  Text(data.overview ?? "unknown",
                      style: Font.txtGreyMediumMD,
                      textDirection: TextDirection.ltr,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
