import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/formatter.dart';

class DetailMoviePage extends StatefulWidget {
  final Results data;
  const DetailMoviePage({Key? key, required this.data}) : super(key: key);

  @override
  State<DetailMoviePage> createState() => _DetailMoviePageState();
}

class _DetailMoviePageState extends State<DetailMoviePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [header(), body()],
          ),
        ));
  }

  Widget header() {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.all(2),
          child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  bottomLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0),
                  bottomRight: Radius.circular(100.0)),
              child: Hero(
                tag: widget.data.id.toString(),
                child: Image.network(
                    "https://image.tmdb.org/t/p/w500/" +
                        widget.data.posterPath!,
                    width: double.infinity,
                    fit: BoxFit.cover),
              )),
        ),
        Positioned(
            top: 30,
            left: 10,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 35,
              ),
            )),
        Positioned(
            bottom: 0,
            child: Container(
              width: MediaQuery.of(context).size.width / 1.5,
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.6),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10))),
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.data.originalTitle ?? "unknown",
                          style: Font.txtBlackBoldLG),
                      Text(
                          widget.data.releaseDate != null
                              ? dateFormatter(
                                  DateTime.parse(widget.data.releaseDate!))
                              : "unknown",
                          style: Font.txtBlackBoldMD),
                    ],
                  )),
            ))
      ],
    );
  }

  Widget body() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text("Rating", style: Font.txtGreyMediumMD),
                  Text(widget.data.voteAverage.toString(),
                      style: Font.txtGreyMediumMD),
                ],
              ),
              Column(
                children: [
                  Text("Popularity", style: Font.txtGreyMediumMD),
                  Text(widget.data.popularity.toString(),
                      style: Font.txtGreyMediumMD),
                ],
              ),
              Column(
                children: [
                  Text("Vote", style: Font.txtGreyMediumMD),
                  Text(widget.data.voteCount.toString(),
                      style: Font.txtGreyMediumMD),
                ],
              ),
            ],
          ),
          SizedBox(height: 10),
          Text(widget.data.overview ?? "", style: Font.txtGreyMediumMD),
        ],
      ),
    );
  }
}
