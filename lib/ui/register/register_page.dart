import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/custom_snackbar.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController();
  final _userNameController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        foregroundColor: Colors.white,
        elevation: 0,
        title: Text("Daftar Akun", style: Font.txtWhiteBoldMD),
      ),
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocRegisterState) {
            customSnackbar(
              context: context,
               message: state.data,
               color: Colors.green
            );
            Navigator.of(context).pop();
          } else if (state is AuthBlocErrorState) {
            customSnackbar(
              context: context,
               message: state.error,
               color: Colors.red
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Daftar Akun',
                  style: Font.txtWhiteBoldBG
                ),
                Text(
                  'Silahkan melakukan pendaftaran akun',
                  style: Font.txtWhiteRegularMD
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleRegister,
                  height: 100,
                  isSecondary: true,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
          ),
          CustomTextFormField(
            context: context,
            controller: _userNameController,
            hint: 'Jhon doe',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final _email = _emailController.textController.text;
    final _userName = _userNameController.textController.text;
    final _password = _passwordController.textController.text;
    if (formKey.currentState!.validate()) {
      User user = User(
        email: _email,
        userName: _userName,
        password: _password,
      );
      context.read<AuthBlocCubit>().registerUser(user);
    }
  }
}
