import 'package:intl/intl.dart';

String dateFormatter(DateTime? date) {
  var dateFormat = DateFormat.yMMMEd().format(date!).toString();
  return dateFormat;
}