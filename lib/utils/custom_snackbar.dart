import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

customSnackbar(
    {required BuildContext context,
    String message = "",
    Color color = Colors.grey}) {
  return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(
      message,
      textAlign: TextAlign.center,
      style: Font.txtWhiteBoldMD,
    ),
    duration: Duration(seconds: 2),
    backgroundColor: color,
  ));
}