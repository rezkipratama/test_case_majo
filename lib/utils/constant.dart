import 'package:flutter/material.dart';

class Preference {
  static const userInfo = "user-info";
}

class Api {
  static const apiKey = "b06932beb868c1ff953e3822e12c4b29";
  static const baseUrl = "https://api.themoviedb.org/3/";
  static const movieList = "trending/all/day";
  static const login = "/login";
  static const register = "/register";
}

class Font {
  static TextStyle txtBlackBoldLG = fontBold(
    size: 17,
    color: Colors.black87,
  );

  static TextStyle txtBlackBoldMD = fontBold(size: 15, color: Colors.black87);

  static TextStyle txtWhiteBoldBG = fontBold(size: 24, color: Colors.white);

  static TextStyle txtWhiteBoldLG = fontBold(size: 17, color: Colors.white);

  static TextStyle txtWhiteBoldMD = fontBold(size: 15, color: Colors.white);

  static TextStyle txtWhiteBoldSM = fontBold(size: 14, color: Colors.white);

  static TextStyle txtGreyMediumMD =
      fontMedium(size: 15, color: Colors.grey[400]);

  static TextStyle txtWhiteRegularMD =
      fontRegular(size: 15, color: Colors.white);

  static TextStyle txtWhiteRegularSM =
      fontRegular(size: 14, color: Colors.white);
}

TextStyle fontBold({Color? color, @required double? size}) {
  return TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w700);
}

TextStyle fontMedium({Color? color, @required double? size}) {
  return TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w600);
}

TextStyle fontRegular({Color? color, @required double? size}) {
  return TextStyle(color: color, fontSize: size);
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
