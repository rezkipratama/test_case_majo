import 'package:majootestcase/di/app_module.dart';
import 'package:majootestcase/local/dao/user_dao.dart';
import 'package:majootestcase/services/repositories/movie_repository.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'package:majootestcase/utils/logger_bloc.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await setupLocator();

  BlocOverrides.runZoned(() {
    runApp(MyApp());
  }, blocObserver: LoggerBlocObserver());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        backgroundColor: Color.fromARGB(255, 14, 13, 13),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (context) =>
            AuthBlocCubit(locator<UserDao>())..fetchHistoryLogin(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(
        buildWhen: (prev, current) =>
            current is AuthBlocLoginState || current is AuthBlocLoggedInState,
        builder: (context, state) {
          if (state is AuthBlocLoginState) {
            return LoginPage();
          } else if (state is AuthBlocLoggedInState) {
            return BlocProvider(
              create: (context) =>
                  HomeBlocCubit(locator<MovieRepository>())..fetchingData(),
              child: HomeBlocScreen(),
            );
          }

          return Center(
              child: Text(kDebugMode ? "state not implemented $state" : ""));
        });
  }
}
