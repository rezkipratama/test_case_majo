import 'package:dio/dio.dart';
abstract class ApiServiceInterface {
  Future<Response> get(String url, {Map<String, dynamic>? queryParameters});
}