import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:majootestcase/services/api_service_interface.dart';
import 'package:majootestcase/utils/error_helper.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class ApiServices implements ApiServiceInterface {
  final Dio _dioInstance;
  ApiServices(this._dioInstance);

  createInstance() async {
    if (kDebugMode) {
      _dioInstance.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 90));
    }
  }

  @override
  Future<Response> get(String url,
      {Map<String, dynamic>? queryParameters}) async {
    try {
      createInstance();
      Response res =
          await _dioInstance.get(url, queryParameters: queryParameters);
      return res;
    } on DioError catch (e) {
      throw ErrorHelper.extractApiError(e);
    } on SocketException {
      throw ErrorHelper.getErrorMessage("No connection");
    }
  }
}
