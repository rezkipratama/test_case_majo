import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/constant.dart';

class MovieRepository {
  final ApiServices _apiServices;
  MovieRepository(this._apiServices);

  Future<MovieModel> getMovieList() async {
    Map<String, dynamic> queryParameters = {
      'api_key': Api.apiKey,
    };
    var response = await _apiServices.get(Api.movieList,
        queryParameters: queryParameters);
    return MovieModel.fromJson(response.data);
  }
}
