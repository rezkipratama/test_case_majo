import 'package:floor/floor.dart';

@entity
class UserEntity {
  @PrimaryKey(autoGenerate: true)
  final int? id;

  final String? email;

  final String? username;

  final String? password;

  UserEntity(
      {
      // ignore: avoid_init_to_null
      this.id = null,
      this.email,
      this.username = "",
      this.password = "",});
}