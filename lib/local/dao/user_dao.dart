import 'package:floor/floor.dart';
import 'package:majootestcase/local/entity/user.dart';

@dao
abstract class UserDao {

  @Query('SELECT * FROM UserEntity WHERE email = :email AND password = :password')
  Future<UserEntity?> login(String email, String password);

  @Query('SELECT * FROM UserEntity WHERE email = :email')
  Future<UserEntity?> checkEmail(String email);

  @insert
  Future<int> register(UserEntity user);
  
}