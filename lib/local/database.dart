import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:majootestcase/local/dao/user_dao.dart';
import 'package:majootestcase/local/entity/user.dart';

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [UserEntity])
abstract class AppDatabase extends FloorDatabase {
  UserDao get userDao;
}