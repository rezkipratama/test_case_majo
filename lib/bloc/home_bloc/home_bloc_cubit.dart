import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/repositories/movie_repository.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  final MovieRepository _movieRepository;
  HomeBlocCubit(this._movieRepository) : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    try {
      final moviesList = await _movieRepository.getMovieList();
      print(moviesList);
      emit(HomeBlocLoadedState(moviesList.results ?? []));
    } catch (error) {
      emit(HomeBlocErrorState(error.toString()));
    }
  }
}
