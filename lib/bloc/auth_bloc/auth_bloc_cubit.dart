import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/local/dao/user_dao.dart';
import 'package:majootestcase/local/entity/user.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  final UserDao _userDao;
  AuthBlocCubit(this._userDao) : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;
    if (!isLoggedIn) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    try {
      final login = await _userDao.login(user.email ?? "", user.password ?? "");
      if (login != null) {
        await sharedPreferences.setBool("is_logged_in", true);
        String data = user.toJson().toString();
        sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocErrorState("Email atau password anda salah!"));
      }
    } catch (error) {
      emit(AuthBlocErrorState(error.toString()));
    }
  }

  void registerUser(User user) async {
    emit(AuthBlocLoadingState());
    try {
      print(user.email);
      final checkEmail = await _userDao.checkEmail(user.email ?? "");
      if (checkEmail == null) {
        await _userDao.register(UserEntity(
            email: user.email,
            username: user.userName,
            password: user.password ?? ""));
        emit(AuthBlocRegisterState("Pendaftaran akun berhasil"));
      } else {
        emit(AuthBlocErrorState("Email telah terdaftar"));
      }
    } catch (error) {
      emit(AuthBlocErrorState(error.toString()));
    }
  }
}
