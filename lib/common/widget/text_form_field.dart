import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:majootestcase/utils/constant.dart';

import 'form_field.dart';

class CustomTextFormField extends CustomFormField<String> {
  CustomTextFormField({
    String? label,
    String? title,
    String? hint,
    required TextController controller,
    required BuildContext context,
    bool enabled = true,
    bool mandatory = true,
    bool isObscureText = false,
    bool isEmail = false,
    bool isDigit = false,
    FormFieldValidator<String>? validator,
    double padding = 4,
    TextInputAction? textInputAction,
    Widget? suffixIcon,
    Key? key,
  }) : super(
          key: key,
          controller: controller,
          enabled: enabled,
          builder: (FormFieldState<String> state) {
            return _CustomTextForm(
              label: label ?? "",
              controller: controller,
              hint: hint ?? "",
              state: state,
              mandatory: mandatory,
              isObscureText: isObscureText,
              suffixIcon: suffixIcon,
              isEmail: isEmail,
              isDigit: isDigit,
              padding: padding,
              textInputAction: textInputAction,
            );
          },
          validator: (picker) {
            if (mandatory && (picker == null || picker.isEmpty)) {
              return 'Form tidak boleh kosong';
            }
            if (isEmail) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (picker != null)
                return pattern.hasMatch(picker)
                    ? null
                    : 'Masukan email yang valid';
            }
            if (validator != null) {
              return validator(picker);
            }
            return null;
          },
        );
}

class TextController extends CustomFormFieldController<String> {
  TextController({String? initialValue}) : super(initialValue = "");

  @override
  String fromValue(String value) => value;

  @override
  String toValue(String text) => text;
}

class _CustomTextForm extends StatefulWidget {
  final FormFieldState<String> state;
  final TextController controller;
  final double padding;
  final String label;
  final String hint;
  final bool mandatory;
  final bool isObscureText;
  final bool isEmail;
  final bool isDigit;
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;

  const _CustomTextForm({
    required this.state,
    required this.controller,
    this.label = "",
    this.hint = "",
    this.padding = 0,
    this.isObscureText = false,
    this.isEmail = false,
    this.mandatory = false,
    this.isDigit = false,
    this.textInputAction,
    this.suffixIcon,
  });

  @override
  State<StatefulWidget> createState() => _CustomTextFormState();
}

class _CustomTextFormState extends State<_CustomTextForm> {
  final _focusNode = FocusNode();

  String get _hint => widget.hint;

  bool get _mandatory => widget.mandatory;

  String get _label {
    var fullLabel = StringBuffer();
    final label = widget.label;
    if (label.isNotEmpty) {
      fullLabel.write(label);
      if (_mandatory) fullLabel.write(' *');
      return fullLabel.toString();
    }
    return label;
  }

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    final inputFormatters = <TextInputFormatter>[];
    TextInputType? keyboardType;

    if (widget.isDigit) {
      inputFormatters.add(FilteringTextInputFormatter.digitsOnly);
      keyboardType = TextInputType.number;
    }
    if (widget.isEmail) {
      keyboardType = TextInputType.emailAddress;
    }
    final _hint = this._hint;
    return Padding(
      padding: EdgeInsets.only(bottom: widget.padding, top: widget.padding),
      child: ConstrainedBox(
        constraints: const BoxConstraints(
          maxHeight: 300,
        ),
        child: LayoutBuilder(
          builder: (context, constraints) {
            return IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _label.isNotEmpty
                      ? Text(_label, style: Font.txtWhiteRegularSM)
                      : SizedBox.shrink(),
                  SizedBox(height: 3),
                  Stack(
                    children: <Widget>[
                      // ignore: unnecessary_null_comparison
                      if (widget.controller.textController == null ||
                          widget.controller.textController.text.isEmpty == true)
                        Positioned(
                            top: _label.isNotEmpty
                                ? 17
                                : _focusNode.hasFocus
                                    ? 17
                                    : 9,
                            left: 10,
                            child: SizedBox(
                              width: constraints.maxWidth * 0.9,
                              child: Text(
                                _hint.isNotEmpty
                                    ? _hint
                                    : 'Fill the field here',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            )),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: TextField(
                          textInputAction: widget.textInputAction,
                          keyboardType: keyboardType,
                          inputFormatters: inputFormatters,
                          focusNode: _focusNode,
                          textAlignVertical:
                              _label.isEmpty ? TextAlignVertical.center : null,
                          controller: widget.controller.textController,
                          style: Font.txtWhiteRegularMD,
                          decoration: defaultInputDecoration.copyWith(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              isDense: _label.isEmpty,
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.always,
                              errorText: widget.state.errorText,
                              alignLabelWithHint: true,
                              hintText: _hint,
                              labelStyle: Font.txtWhiteRegularMD,
                              hintStyle: TextStyle(color: Colors.white.withOpacity(0.3)),
                              suffixIcon: _focusNode.hasFocus
                                  ? widget.suffixIcon ??
                                      IconButton(
                                        icon: const Icon(Icons.cancel),
                                        onPressed: () {
                                          widget.controller.textController
                                              .clear();
                                        },
                                      )
                                  : null),
                          obscureText: widget.isObscureText,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
