// Mocks generated by Mockito 5.1.0 from annotations
// in majootestcase/test/bloc/auth_bloc_test.dart.
// Do not manually edit this file.

import 'dart:async' as _i3;

import 'package:majootestcase/local/dao/user_dao.dart' as _i2;
import 'package:majootestcase/local/entity/user.dart' as _i4;
import 'package:mockito/mockito.dart' as _i1;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types

/// A class which mocks [UserDao].
///
/// See the documentation for Mockito's code generation for more information.
class MockUserDao extends _i1.Mock implements _i2.UserDao {
  MockUserDao() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i3.Future<_i4.UserEntity?> login(String? email, String? password) =>
      (super.noSuchMethod(Invocation.method(#login, [email, password]),
              returnValue: Future<_i4.UserEntity?>.value())
          as _i3.Future<_i4.UserEntity?>);
  @override
  _i3.Future<_i4.UserEntity?> checkEmail(String? email) =>
      (super.noSuchMethod(Invocation.method(#checkEmail, [email]),
              returnValue: Future<_i4.UserEntity?>.value())
          as _i3.Future<_i4.UserEntity?>);
  @override
  _i3.Future<int> register(_i4.UserEntity? user) =>
      (super.noSuchMethod(Invocation.method(#register, [user]),
          returnValue: Future<int>.value(0)) as _i3.Future<int>);
}
