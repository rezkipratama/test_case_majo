import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:mockito/mockito.dart';

import '../core/repository/movie_repository.mocks.dart';

void main() {
  late HomeBlocCubit homeBlocCubit;
  late MockMovieRepository movieRepository;
  MovieModel u200 = MovieModel(page: 1, results: [
      Results(
          id: 634649,
          originalTitle: "Spider-Man: No Way Home",
          releaseDate: "2021-12-15",
          voteAverage: 8.2,
          overview:
              "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.",
          voteCount: 10112,
          popularity: 11672.708)
    ]);

  setUp(() {
    movieRepository = MockMovieRepository();
    homeBlocCubit = HomeBlocCubit(movieRepository);
  });

  group("HomeBlocCubit Test", () {
    blocTest<HomeBlocCubit, HomeBlocState>(
        "get data movie list failed when method is fetchingData screnario",
        build: () {
          
          when(movieRepository.getMovieList())
              .thenThrow("Error");
          return homeBlocCubit;
        },
        act: (cubit) => cubit.fetchingData(),
        expect: () => [HomeBlocLoadingState(),HomeBlocErrorState("Error")]);
    blocTest<HomeBlocCubit, HomeBlocState>(
        "get data movie list success when method is fetchingData screnario",
        build: () {
          
          when(movieRepository.getMovieList())
              .thenAnswer((realInvocation) => Future.value(u200));
          return homeBlocCubit;
        },
        act: (cubit) => cubit.fetchingData(),
        expect: () => [HomeBlocLoadingState(),HomeBlocLoadedState(u200.results ?? [])]);
  });

  tearDown(() {
    homeBlocCubit.close();
  });
}
