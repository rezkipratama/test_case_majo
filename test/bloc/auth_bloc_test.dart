import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/local/dao/user_dao.dart';
import 'package:majootestcase/local/entity/user.dart';
import 'package:majootestcase/models/user.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'auth_bloc_test.mocks.dart';

@GenerateMocks([UserDao])
void main() {
  late AuthBlocCubit authBlocCubit;
  late MockUserDao userDao;
  late SharedPreferences preference;
  SharedPreferences.setMockInitialValues({
    "is_logged_in": true,
  });
  String email = "johndoe@gmail.com";
  String password = "pass123";
  UserEntity u200 = UserEntity(
      email: "example@gmail.com", username: "Johndoe", password: "pass123");

  setUp(() async {
    userDao = MockUserDao();
    preference = await SharedPreferences.getInstance();
    authBlocCubit = AuthBlocCubit(userDao);
  });

  group("AuthBlocCubit Test", () {
    blocTest<AuthBlocCubit, AuthBlocState>(
        "auth success when method is fetchHistoryLogin screnario",
        build: () {
          expect(true, preference.getBool("is_logged_in"));
          return authBlocCubit;
        },
        act: (cubit) => cubit.fetchHistoryLogin(),
        expect: () => [AuthBlocInitialState(), AuthBlocLoggedInState()]);

    blocTest<AuthBlocCubit, AuthBlocState>(
        "auth login success when method is login screnario",
        build: () {
          when(userDao.login(email, password))
              .thenAnswer((realInvocation) => Future.value(u200));
          expect(true, preference.getBool("is_logged_in"));
          return authBlocCubit;
        },
        act: (cubit) => cubit.loginUser(User(email: email, password: password)),
        expect: () => [AuthBlocLoadingState(), AuthBlocLoggedInState()]);

  });

  tearDown(() {
    authBlocCubit.close();
  });
}
