import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/repositories/movie_repository.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'movie_repository.mocks.dart';

@GenerateMocks([MovieRepository])
void main() {
  group('getMovieList Test', () {

    MovieModel u200 = MovieModel(page: 1, results: [
      Results(
          id: 634649,
          originalTitle: "Spider-Man: No Way Home",
          releaseDate: "2021-12-15",
          voteAverage: 8.2,
          overview:
              "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.",
          voteCount: 10112,
          popularity: 11672.708)
    ]);

    MovieModel u404 = MovieModel(page: 1, results: []);

    test('get movie list success scenario', () async {
      final networkRepository = MockMovieRepository();

      when(networkRepository.getMovieList())
          .thenAnswer((realInvocation) => Future.value(u200));

      var res = await networkRepository.getMovieList();
      expect(u200.results, res.results);
    });

    test('get movie list empty scenario', () async {
      final networkRepository = MockMovieRepository();

      when(networkRepository.getMovieList())
          .thenAnswer((realInvocation) => Future.value(u404));

      var res = await networkRepository.getMovieList();
      expect(u404.results, res.results);
    });
  });
}
