import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/user.dart';

main() {
  group('User Model', () {
    User u200 = User(
      email: "example@gmail.com",
      userName: "John doe",
      password: "pass123"
    );
        

    User u404 = User(
      email: "",
      userName: "",
      password: ""
    );

    test('email is valid', () {
      expect(u200.email, equals('example@gmail.com'));
    });

    test('username is valid', () {
      expect(u200.userName, equals('John doe'));
    });

    test('password is valid', () {
      expect(u200.password, equals('pass123'));
    });

    test('email is empty', () {
      expect(u404.email, equals(''));
    });

    test('username is empty', () {
      expect(u404.userName, equals(''));
    });

    test('password is empty', () {
      expect(u404.password, equals(''));
    });
  });
}