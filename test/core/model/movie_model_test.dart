import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie_model.dart';

main() {
  group('User Model', () {
    MovieModel u200 = MovieModel(page: 1, results: [
      Results(
          id: 634649,
          originalTitle: "Spider-Man: No Way Home",
          releaseDate: "2021-12-15",
          voteAverage: 8.2,
          overview:
              "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.",
          voteCount: 10112,
          popularity: 11672.708)
    ]);

    MovieModel u404 = MovieModel(page: 1, results: []);

    test('result has data', () {
      expect(u200.results, equals(u200.results));
    });

    test('result is empty', () {
      expect(u404.results, equals([]));
    });

    test('id is valid', () {
      expect(u200.results![0].id, equals(634649));
    });

    test('originalTitle is valid', () {
      expect(u200.results![0].originalTitle, equals("Spider-Man: No Way Home"));
    });

    test('releaseDate is valid', () {
      expect(u200.results![0].releaseDate, equals("2021-12-15"));
    });

    test('voteAverage is valid', () {
      expect(u200.results![0].voteAverage, equals(8.2));
    });

    test('overview is valid', () {
      expect(
          u200.results![0].overview,
          equals(
              "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man."));
    });

    test('voteCount is valid', () {
      expect(u200.results![0].voteCount, equals(10112));
    });

    test('popularity is valid', () {
      expect(u200.results![0].popularity, equals(11672.708));
    });
  });
}
